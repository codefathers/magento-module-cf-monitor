<?php

/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */
class Cf_Monitor_Block_Cart_Details extends Cf_Monitor_Block_Table
{
    const SEPARATOR = 'separator';

    private $sections = null;

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('cf-monitor/cart_details.phtml');
    }

    public function getSections()
    {
        if (!isset($this->sections)) {


            $quote = $this->getQuote();


            $this->sections = array();


            $this->sections[] = array(
                'name' => 'Quote',
                'type' => $this->_getTypeString($quote),
                'values' => $quote->getData()
            );
            $this->sections[] = array(
                'name' => 'Billing',
                'type' => $this->_getTypeString($quote->getBillingAddress()),
                'values' => $quote->getBillingAddress()->getData()
            );
            $this->sections[] = array(
                'name' => 'Shipping',
                'type' => $this->_getTypeString($quote->getShippingAddress()),
                'values' => $quote->getShippingAddress()->getData()
            );
            $this->sections[] = array(
                'name' => 'Payment',
                'type' => $this->_getTypeString($quote->getPayment()),
                'values' => $quote->getPayment()->getData()
            );

            $this->sections[] = self::SEPARATOR;

            $items = $quote->getAllItems();
            foreach ($items as $item) {

                $options = $item->getProductOptions();
                $this->sections[] = array(
                    'name' => 'Artikel ' . ((integer) $item->getQty()) . 'x ' . $item->getName(),
                    'type' => $this->_getTypeString($item),
                    'values' => $item->getData()
                );

                $options = $item->getOptions();
                $list = array();
                foreach ($options as $option) {
                    $list[$option->getCode()] = $option->getValue();
                }

                $this->sections[] = array(
                    'name' => 'Optionen ' . ((integer) $item->getQtyOrdered()) . 'x ' . $item->getName(),
                    'values' => $list
                );

                $this->sections[] = self::SEPARATOR;
            }
        }
        return $this->sections;
    }

}