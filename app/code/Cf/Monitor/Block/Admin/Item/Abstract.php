<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */




class Cf_Monitor_Block_Admin_Item_Abstract extends Mage_Core_Block_Template
{
    


    /**
     *
     *
     * renders values into a key/value html table
     *
     * @param array $values
     * @return string
     * 
     */
    public function getValueTableHtml($values) {
        $block = $this->getLayout()->getBlock('monitor.value.table');
        $result = clone $block;
        $result->setValues($values);
        return $result->toHtml();
    }
    


}


