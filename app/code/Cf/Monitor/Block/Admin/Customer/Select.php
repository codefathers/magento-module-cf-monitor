<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */



class Cf_Monitor_Block_Admin_Customer_Select extends Mage_Adminhtml_Block_Template
{

    protected $_customers = null;
    
    public function getCustomers ()
    {
        if (!isset($this->_customers)) {
           $this->_customers = Mage::getModel('customer/customer')->getCollection();
           $this->_customers->addAttributeToSelect('*');
           $this->_customers->setOrder(array('lastname','firstname'),Varien_Data_Collection_Db::SORT_ORDER_ASC);
           $this->_customers->load();
        }
        return $this->_customers;

    }



    
}








