<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */



class Cf_Monitor_Block_Admin_View extends Mage_Core_Block_Template
{
    
    



    public function getItemsHtml()
    {
        $renderer = $this->getChild('items');
        $renderer->setItems($this->getItems());
        return $renderer->toHtml();
    }


    
}








