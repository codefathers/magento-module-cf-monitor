<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */





class Cf_Monitor_Block_Admin_Items extends Mage_Core_Block_Template
{
    
    


    /**
     *
     * renders an item to html
     *
     * @param  Cf_Monitor_Model_Data_Item_Abstract $item
     * @return string
     */
    public function getItemHtml($item)
    {
        $renderer = $this->_getRenderer($item);
        $renderer->setItem($item);
        return $renderer->toHtml();
    }



    /**
     *
     * returns an item renderer block
     *
     * @param Cf_Monitor_Model_Data_Item_Abstract $item
     * @return Cf_Monitor_Block_Admin_Item_Abstract
     */
    protected function _getRenderer($item)
    {
        $block = null;
        if ($item instanceof Cf_Monitor_Model_Data_Item_Separator) {
            $block = $this->getChild('renderer.separator');
        } else {
            $block = $this->getChild('renderer.default');
        }
        return ($block)?clone $block:null;
    }


    
}








