<?php

/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */
class Cf_Monitor_Block_Customer_Details extends Cf_Monitor_Block_Table
{
    const SEPARATOR = 'separator';
    private $sections = null;

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('cf-monitor/customer_details.phtml');
    }

    public function getSections()
    {
        if (!isset($this->sections)) {
            $this->sections = Array();
            $customer = $this->getCustomer();

            $this->sections[] = Array(
                'name' => 'Customer',
                'type' => $this->_getTypeString($customer),
                'values' => $customer->getData()
            );


            $addresses = $customer->getAddresses();
            foreach ($addresses as $address) {
                $this->sections[] = Array(
                    'name' => 'Address',
                    'type' => $this->_getTypeString($address),
                    'values' => $address->getData()
                );
            }
        }
        return $this->sections;
    }

}

