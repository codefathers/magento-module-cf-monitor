<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */


class Cf_Monitor_Block_Table extends Cf_Monitor_Block_Template
{



    public function __construct() {
        parent::__construct();
        $this->setTemplate('cf-monitor/table.phtml');
    }



 

    public function getTableHtml($values) {
        return $this->getLayout()->createBlock('cf_monitor/table',null,array('values'=>$values))->toHtml();
    }
    
    
    protected function _getTypeString($obj) 
    {
        if (is_object($obj)) {
            return get_class($obj);
        } elseif (is_array($obj)) {
            return 'array';
        } elseif (is_string($obj)) {
            return 'string';
        }
        return '';
    }
    
       

    
}







