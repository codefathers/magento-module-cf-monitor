<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */

class Cf_Monitor_Block_Order_Details extends Cf_Monitor_Block_Table
{

    
    const SEPARATOR = 'separator';
    private $sections = null;


    public function __construct() {
        parent::__construct();
        $this->setTemplate('cf-monitor/order_details.phtml');
    }



    public function getSections() {
        if (!isset($this->sections)) {
            $this->sections = Array();
            $order = $this->getOrder();
            
            
            $this->sections[] = Array(
                'name'=>'Order',
                'type'=>$this->_getTypeString($order),
                'values'=>$order->getData()
            );
            if ($order->getBillingAddress()) {
                $this->sections[] = Array(
                    'name'=>'Billing',
                    'type'=>$this->_getTypeString($order->getBillingAddress()),
                    'values'=>$order->getBillingAddress()->getData()
                );
            };
            if ($order->getShippingAddress()) {
                $this->sections[] = Array(
                    'name'=>'Shipping',
                    'type'=>$this->_getTypeString($order->getShippingAddress()),
                    'values'=>$order->getShippingAddress()->getData()
                );
            }
            if ($order->getPayment()) {
                $this->sections[] = Array(
                    'name'=>'Payment',
                    'type'=>$this->_getTypeString($order->getPayment()),
                    'values'=>$order->getPayment()->getData()
                );
            };

            $this->sections[] = self::SEPARATOR;
            
            $items = $order->getAllItems();
            foreach ($items as $item) {
                $this->sections[] = Array(
                    'name'=>'Artikel '.((integer) $item->getQtyOrdered()).'x '.$item->getName(),
                    'type'=>$this->_getTypeString($item),
                    'values'=>$item->getData()
                );
            }


        }
        return $this->sections;
    }



    



    
}






