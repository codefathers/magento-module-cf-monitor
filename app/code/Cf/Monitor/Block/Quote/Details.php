<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */

class Cf_Monitor_Block_Quote_Details extends Cf_Monitor_Block_Table
{

    
    const SEPARATOR = 'separator';
    private $sections = null;


    public function __construct() {
        parent::__construct();
        $this->setTemplate('cf-monitor/quote_details.phtml');
    }



    public function getSections() {
        if (!isset($this->sections)) {
            $this->sections = Array();
            $quote = $this->getQuote();
            
            
            $this->sections[] = Array(
                'name'=>'Quote',
                'type'=>$this->_getTypeString($quote),
                'values'=>$quote->getData()
            );

            if ($quote->getBillingAddress()) {
                $this->sections[] = Array(
                    'name'=>'Billing',
                    'type'=>$this->_getTypeString($quote->getBillingAddress()),
                    'values'=>$quote->getBillingAddress()->getData()
                );
            }

            if ($quote->getShippingAddress()) {
                $this->sections[] = Array(
                    'name'=>'Shipping',
                    'type'=>$this->_getTypeString($quote->getShippingAddress()),
                    'values'=>$quote->getShippingAddress()->getData()
                );
            }

            if ($quote->getPayment()) {
                $this->sections[] = Array(
                    'name'=>'Payment',
                    'type'=>$this->_getTypeString($quote->getPayment()),
                    'values'=>$quote->getPayment()->getData()
                );
            };

            $this->sections[] = self::SEPARATOR;
            
            $items = $quote->getAllItems();
            foreach ($items as $item) {
                $this->sections[] = Array(
                    'name'=>'Artikel '.((integer) $item->getQtyOrdered()).'x '.$item->getName(),
                    'type'=>$this->_getTypeString($item),
                    'values'=>$item->getData()
                );
            }
            
            

        }
        return $this->sections;
    }



    



    
}






