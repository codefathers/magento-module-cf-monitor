<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */


abstract class Cf_Monitor_Model_Data_Collection_Abstract extends Cf_Core_Model_Collection
{

    protected $_model = null;

    /**
     * constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->_model = null;
        $this->setItemModel('cf_monitor/data_item_default');
    }
    

    /**
     * sets the data model
     * @param Varien_Object $model
     */
    protected function _setModel(Varien_Object $model)
    {
        $this->clear();
        if ($this->_model = $model) {
            $this->load();
        }
    }


    /**
     * sets the data model
     *
     * @return Varien_Object
     */
    protected function _getModel()
    {
        return $this->_model;
    }


    /**
     * loads the data items
     *
     */
    public function load()
    {
        $this->clear();
        $this->_load();
    }


    /**
     * loads the data items
     *
     */
    public function addSeparator()
    {
        $item = Mage::getModel('cf_monitor/data_item_separator');
        return $this->addItem($item);
    }
    

    /**
     * internally loads the data items
     *
     */
    abstract protected function _load();


}
