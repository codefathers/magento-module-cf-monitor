<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */



class Cf_Monitor_Model_Data_Collection_Customer extends Cf_Monitor_Model_Data_Collection_Abstract
{

    /**
     * constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    public function setCustomer(Mage_Customer_Model_Customer $customer)
    {
        $this->_setModel($customer);
    }

    /**
     * @return Mage_Checkout_Model_Cart
     */
    public function getCustomer()
    {
        return $this->_getModel();
    }


    /**
     * internally loads the data items
     *
     */
    protected function _load()
    {
        $customer = $this->getCustomer();

        $section = $this->addItem();
        $section->setName(Mage::helper('cf_monitor')->__('Customer'));
        $section->setValues($customer->getData());

        $addresses = $customer->getAddresses();
        foreach ($addresses as $address) {
            $section = $this->addItem();
            $section->setName(Mage::helper('cf_monitor')->__('Address %s',$address->getId()));
            $section->setValues($address->getData());
        }


    }
   


}
