<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */


class Cf_Monitor_Model_Data_Collection_Order extends Cf_Monitor_Model_Data_Collection_Abstract
{

    /**
     * constructor
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     *
     */
    public function setOrder(Mage_Sales_Model_Order $customer)
    {
        $this->_setModel($customer);
    }

    /**
     * @return Mage_Checkout_Model_Cart
     */
    public function getOrder()
    {
        return $this->_getModel();
    }


    /**
     * internally loads the data items
     *
     */
    protected function _load()
    {
        
    }


}
