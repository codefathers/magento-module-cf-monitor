<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */


class Cf_Monitor_Model_Data_Collection_Cart extends Cf_Monitor_Model_Data_Collection_Abstract
{

    /**
     * constructor
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     *
     */
    public function setQuote(Mage_Sales_Model_Quote $quote)
    {
        $this->_setModel($quote);
    }

    /**
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->_getModel();
    }

    /**
     * 
     *//*
    public function setCart(Mage_Checkout_Model_Cart $cart)
    {
        $this->_setModel($cart);
    }*/

    /**
     * @return Mage_Checkout_Model_Cart
     */
    /*
    public function getCart()
    {
        return $this->_getModel();
    }*/

    /**
     * internally loads the data items
     *
     */
    protected function _load()
    {
        //$cart = $this->getCart();

        /*$section = $this->addItem();
        $section->setName(Mage::helper('cf_monitor')->__('Cart'));
        $section->setValues($cart->getData());
         * 
         */

        $quote = $this->getQuote();
        $section = $this->addItem();
        $section->setName(Mage::helper('cf_monitor')->__('Quote'));
        $section->setValues($quote->getData());

        $section = $this->addItem();
        $section->setName(Mage::helper('cf_monitor')->__('Shipping'));
        $section->setValues($quote->getShippingAddress()->getData());

        $section = $this->addItem();
        $section->setName(Mage::helper('cf_monitor')->__('Billing'));
        $section->setValues($quote->getBillingAddress()->getData());

        $section = $this->addItem();
        $section->setName(Mage::helper('cf_monitor')->__('Payment'));
        $section->setValues($quote->getPayment()->getData());
        
        $this->addSeparator();
        
        $items = $quote->getAllItems();
        foreach ($items as $item) {
            $section = $this->addItem();
            $name = ((integer) $item->getQty()).'x '.$item->getName();
            $section->setName(Mage::helper('cf_monitor')->__('Product %s',$name));
            $section->setValues($item->getData());

            $section = $this->addItem();
            $options = $item->getOptions();
            $list = Array();
            foreach ($options as $option) {
               $list[$option->getCode()] = $option->getValue();
            }
            $name = ((integer) $item->getQtyOrdered()).'x '.$item->getName();
            $section->setName(Mage::helper('cf_monitor')->__('Options %s',$name));
            $section->setValues($list);
            
            $this->addSeparator();
        }


    }


}
