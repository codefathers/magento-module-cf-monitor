<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */


class Cf_Monitor_Controller_Front extends Mage_Core_Controller_Front_Action
{





    /**
     * Predispatch: shoud set layout area
     *
     * @return Mage_Core_Controller_Front_Action
     */
    public function preDispatch_XXXXXXXXXXXXXXXXXXXXXXXXXx()
    {
	
        if ($this->_request->getClientIp() !== $_SERVER['SERVER_ADDR']) {
            throw new Exception ("Can't handle request");
        }
        
        if (!$this->_isLoggedInIntoAdmin()) {
            throw new Exception ("Please log in");
        }
	
        
        /*
        $admin = Mage::getSingleton('admin/session');
        var_dump($admin->getData());
        if (!$admin->isLoggedIn()) {
        }
         * 
         */
        return parent::preDispatch();
    }
    
    /**
     * 
     */
    protected function _isLoggedInIntoAdmin()
    {
        return true;
    }




}