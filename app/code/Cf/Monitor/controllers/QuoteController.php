<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */

class Cf_Monitor_QuoteController extends Cf_Monitor_Controller_Front
{
    
    
    
    private $_profiles = Array();
    
    
    
    
    
    
    public function _construct() {
        $this->_profiles = Array();
    }


    public function indexAction()
    {
        return $this->detailsAction();
    }

    

    public function detailsAction()
    {
        if (!$id = Mage::app()->getRequest()->getParam('id'))
            throw new Exception ("bitte eine id angeben..");

       $quote = Mage::getModel('sales/quote');
       $quote->load($id);

       if (!$quote->getId()) {
            throw new Exception ("Der Quote id=$id existiert nicht");
       }

       
       $block = $this->getLayout()->createBlock('cf_monitor/quote_details','quote.details');
       $block->setQuote($quote);
       
       

       echo $block->toHtml();
    }
    
        
    
    

    
    
    





}