<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */



class Cf_Monitor_CartController extends Cf_Monitor_Controller_Front
{


    public function indexAction()
    {
        return $this->detailsAction();
    }

    protected function getQuote()
    {
        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();
        return $quote;
    }

    public function updateActionXXXXX()
    {
        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();
        $quote->collectTotals();
        $quote->save();
        echo "done";
    }

    public function detailsAction()
    {
        $block = $this->getLayout()->createBlock('cf_monitor/cart_details', 'cart.details');
        $block->Quote = $this->getQuote();
        $block->Quote->collectTotals();
        echo $block->toHtml();
    }

}