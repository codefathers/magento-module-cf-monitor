<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */


class Cf_Monitor_CustomerController extends Cf_Monitor_Controller_Front
{
    
    
    
    private $_profiles = Array();
    
    
    
    public function _construct() {
        $this->_profiles = Array();
    }


    public function indexAction()
    {
        return $this->detailsAction();
    }

    

    public function detailsAction()
    {
        if (!$id = Mage::app()->getRequest()->getParam('id'))
            throw new Exception ("bitte eine id angeben..");

       $customer = Mage::getModel('customer/customer');
       $customer->load($id);

       if (!$customer->getId())
            throw new Exception ("Der Kunde id=$id existiert nicht");

       
       $block = $this->getLayout()->createBlock('cf_monitor/customer_details','customer.details');
       $block->setCustomer($customer);
       
       

       echo $block->toHtml();
    }
    
        
    
    

    
    
    





}