<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */

class Cf_Monitor_Admin_CustomerController extends Cf_Monitor_Controller_Admin
{

    /**
     * Index action
     */
    public function indexAction()
    {
        return $this->detailsAction();
    }

    /**
     * details action
     */
    public function detailsAction()
    {
        $this->loadLayout();

        $collection = Mage::getModel('cf_monitor/data_collection_customer');
        $collection->setTitle(Mage::helper('cf_monitor')->__('Customer'));
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $customer = Mage::getModel('customer/customer')->load($id);
            if ($customer->getId()) {
                $collection->setCustomer($customer);
            }
        }


        $block = $this->getLayout()->getBlock('content')->getChild('monitor');
        $block->setItems($collection);

        $block = $this->getLayout()->getBlock('left')->getChild('customer.select');
        $block->setItems($collection);

        $this->renderLayout();

        $this->renderLayout();
    }

}