<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */


class Cf_Monitor_Admin_OrderController extends Cf_Monitor_Controller_Admin
{
    
    
    

    /**
    * Index action
    */
    public function indexAction()
    {
        return $this->detailsAction();
    }




    /**
    * details action
    */
    public function detailsAction()
    {
        $this->loadLayout();

        $collection = Mage::getModel('cf_monitor/data_collection_order');
        $collection->setTitle(Mage::helper('cf_monitor')->__('Order'));
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $order = Mage::getModel('sales/order')->load($id);
            if ($order->getId()) {
                $collection->setOrder($order);
            }
        }


        $block = $this->getLayout()->getBlock('content')->getChild('monitor');
        $block->setItems($collection);

        $this->renderLayout();
    }




}