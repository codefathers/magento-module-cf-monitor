<?php
/**
 * Cf Monitor magento module
 *
 * LICENSE
 *
 *
 * @copyright  Copyright (c) 1999-2011 codefathers www.codefathers.com
 * @author Joachim Schweisgut <a.schweisgut@codefathers.de>
 * @category Telekom
 * @package Cf_Monitor
 */


class Cf_Monitor_Admin_CartController extends Cf_Monitor_Controller_Admin
{

    
    /**
    * Index action
    */
    public function indexAction()
    {
        return $this->detailsAction();
    }


    /**
    * details action
    */
    public function detailsAction()
    {
        
        $quote = Mage::getModel('sales/quote')->setStoreId(1);
        $quote->load(1969);

        $collection = Mage::getModel('cf_monitor/data_collection_cart');
        $collection->setTitle(Mage::helper('cf_monitor')->__('Cart'));
        $collection->setQuote($quote);

        $this->loadLayout();

        $block = $this->getLayout()->getBlock('content')->getChild('monitor');
        $block->setItems($collection);

        $this->renderLayout();
    }
    

}